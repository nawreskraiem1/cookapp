const express = require("express");
const router = express.Router();

const platsController = require("../controllers/platsController");

router.route("/").get(platsController.getPlats);
router.route("/").post(platsController.createPlat);
//router.route("/").get(platsController.getDay);

module.exports = router;
