const Plats = require("../Models/platModel");
const catchAsync = require("../utils/catchAsync");
const fetch = require("node-fetch");

exports.createPlat = catchAsync(async (req, res) => {
  const plat = await Plats.create(req.body);
  res.status(201).json({
    status: "success",
    data: {
      plat,
    },
  });
});

exports.getPlats = catchAsync(async (req, res) => {
  const ingredient = req.query.ingredient;
  const plat = await Plats.find({ ingredients: ingredient });
  res.status(201).json({
    status: "success",
    data: {
      plat,
    },
  });
});

const ConvertDate = (str) => {
  const heur = str.split(":")[0];
  const min = str.split(":")[1];
  return parseInt(heur) * 60 + parseInt(min);
};

exports.getPlats = catchAsync(async (req, res) => {
  const { ingredient } = req.query;
  var { DayPar } = req.query;
  const plat = await Plats.find({ ingredients: ingredient, day: DayPar });

  const API = fetch(
    "http://api.aladhan.com/v1/calendar?latitude=51.508515&longitude=-0.1254872&method=1&month=9&year=1440"
  );

  API.then((res) => res.json()).then((json) => {
    json.data.map((day) => {
      if (day.date.gregorian.day == DayPar) {
        hourAsr = ConvertDate(day.timings.Asr);
        hourMaghrib = ConvertDate(day.timings.Maghrib);
        console.log("hourAsr", hourAsr);
        console.log("hourMaghrib", hourMaghrib);
        diff = hourMaghrib - hourAsr - 15;
        console.log("diff", diff);
      }
    });
  });

  if (plat.duration > diff) {
    const x = plat.duration - diff;
    console.log(x, "minut after Asr ");
  }

  res.status(201).json({
    status: "success",
    data: {
      plat,
    },
  });
});
