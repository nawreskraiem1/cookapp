const express = require("express");
const morgan = require("morgan");
const app = express();
const platRouter = require("./routes/platRouter");


app.use(express.json());

app.use("/api/v1/cooktime", platRouter);



app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
});

module.exports = app;
