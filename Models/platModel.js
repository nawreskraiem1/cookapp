const mongoose = require("mongoose");
const validator = require("validator");

const platSchema = new mongoose.Schema({
  name: {
    type: String,
    // required: [true, "Please tell us the plat name!"],
    // unique: [true, "this username already exists"],
  },

  ingredients: [
    {
      type: String,
      //required: [true, "Please tell us the ingredient name!"],
    },
  ],

  duration: {
    type: Date,
    //required: [true, "Please tell us the duration of the plat !"],
  },
  Day: {
    type: Date,
  },
});

const Plat = mongoose.model("Plats", platSchema);
module.exports = Plat;
